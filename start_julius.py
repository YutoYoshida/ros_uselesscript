#!/usr/bin/env python 
import os
import rospy

def kill():
	os.system("killall julius")
	
os.chdir(os.path.dirname(__file__) + "/../etc")
rospy.init_node("start_julius")
rospy.on_shutdown(kill)
os.system("julius -C command.jconf -input mic")

rospy.spin()
