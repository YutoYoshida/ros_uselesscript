#!/usr/bin/env python
#encoding: utf8

import socket,os
from time import sleep
import re
import threading
import sys
import rospy
import difflib # 類似度を計算するライブラリ


class JuliusReceiver:
	def __init__(self):
		global client
		rate = rospy.Rate(10)
		while not rospy.is_shutdown():
			try:
				client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
				client.connect(("localhost",10500))
				break
			except:
				rate.sleep()

		rospy.on_shutdown(client.close)

	def get_line(self):
		line = ""
		while not rospy.is_shutdown():
			v = client.recv(1)
			if (v == "\n"):
				return line
			line += v

	def get_command(self, th):
		line = self.get_line()
		#client.send('PAUSE\n')
		if("WHYPO" not in line):
			return None

		score_str = line.split('CM="')[-1].split('"')[0]
		# 閾値を超えるか超えないか
		if(float(score_str)) < th:
			client.send('PAUSE\n')
			sleep(3)
			client.send('RESUME\n')
			return "認識精度->"+str(score_str)
		else:
			return line

def get_similar(listX, listY):
    s = difflib.SequenceMatcher(None, listX, listY).ratio()
    return s

if __name__ == "__main__":
	rospy.init_node("voice_to_command")
	j = JuliusReceiver()
	#client.send('PAUSE\n')
	while not rospy.is_shutdown():
		com = j.get_command(0.999)
		if (com != None):
			print(com)



