#!/usr/bin/env python
# -*- coding: utf-8 -*-
import rospy
import cv2
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError
import numpy as np
 
class To_OpenCV():
    def __init__(self):
        sub = rospy.Subscriber("/camera/depth/image_raw", Image, self.get_image)
        self.bridge = CvBridge()  
        self.image_org = None  
 
    def get_image(self, img):
        try:
            self.image_org = self.bridge.imgmsg_to_cv2(img, "passthrough") 
        except CvBridgeError, e:
            print e
 
    def detect_image(self):
        if self.image_org is None:
            return None
        else:
            print(self.image_org[160][120])
 
 
if __name__ == '__main__':
    rospy.init_node('To_OpenCV')
    fd = To_OpenCV()
 
    rate = rospy.Rate(5)  
    while not rospy.is_shutdown():
        rospy.loginfo(fd.detect_image())
        rate.sleep()
