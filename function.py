#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import rospy
from std_msgs.msg import String

def function1():
	print("function1")

def function2():
	print("function2")

def topicDetector():
	# ここが無限ループで実行される
	print("hogehoge")
	rospy.Subscriber("talker", String, callback1)
	rospy.Subscriber("chatter", String, callback2)

def callback1(msg):
	print(msg.data)
	if(msg.data == "hello"):
		function1()
	if(msg.data == "bye"):
		function2()
def callback2(msg):
	print("this is chatter")
	print("hoge")

if __name__ == '__main__':
	rospy.init_node("rosFunction")
	rate = rospy.Rate(1)
	while not rospy.is_shutdown():
		topicDetector()
		rate.sleep()