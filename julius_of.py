#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Help-me-carry,音声班,publisher

import re
import socket
import sys
from time import sleep

import rospy
from std_msgs.msg import Bool, String


def connection():
	while not rospy.is_shutdown():
		try:
			global client
			HOST = "localhost"  # アドレス
			PORT = 10500  # ポート
			client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
			client.connect((HOST, PORT))
			client.send('RESUME\n')
			break
		except socket.error:
			sys.stdout.write('.')

def control(message):
	global client

	if message.data == True:
		client.send('RESUME\n')

	if message.data == False:
		client.send('PAUSE\n')

if __name__ == "__main__":
	rospy.init_node("julius_of")
	try:
		rospy.Subscriber('main_recognition_control', Bool, control)
		connection()
	except rospy.ROSInterruptException: pass