#! /usr/bin/env python

# -*- coding: utf-8 -*-

import rospy
from sensor_msgs.msg import Joy
from geometry_msgs.msg import Twist
class JoyPrint(object):
	def __init__(self):
		self.joy_sub = rospy.Subscriber('joy',Joy,self.joy_callback,queue_size=1)
		self.twist_pub = rospy.Publisher('/turtle1/cmd_vel',Twist,queue_size=1)

	def joy_callback(self, joy_msg):
		r = rospy.Rate(5)
		twist = Twist()
		if (joy_msg.buttons[0]==1 & joy_msg.buttons[6] == 1):
			twist.linear.x = joy_msg.axes[1] * 10.0
			twist.angular.z = joy_msg.axes[0] * 5.0
			self.twist_pub.publish(twist)
		elif joy_msg.buttons[0] == 1:
			twist.linear.x = joy_msg.axes[1] * 0.5
			twist.angular.z = joy_msg.axes[0] * 1.0
			self.twist_pub.publish(twist)
		elif joy_msg.buttons[1] == 1:
			twist.linear.x = joy_msg.buttons[1] * 100.0
			self.twist_pub.publish(twist)
		elif joy_msg.buttons[2] == 1:
			twist.linear.x = joy_msg.buttons[2] * 100.0
			twist.angular.z = joy_msg.buttons[2] * -50.0
			self.twist_pub.publish(twist)
		elif joy_msg.buttons[3] == 1:
			twist.angular.z = joy_msg.buttons[3] * 100.0
			self.twist_pub.publish(twist)

		
if __name__ == '__main__':
	rospy.init_node('joymove')
	rate = rospy.Rate(5)
	while not rospy.is_shutdown():
		joy_twist = JoyPrint()
		rate.sleep()
