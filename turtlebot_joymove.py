#! /usr/bin/env python

# -*- coding: utf-8 -*-

import os,sys
import rospy
from sensor_msgs.msg import Joy
from geometry_msgs.msg import Twist
class JoyMove(object):
	def __init__(self):
		self.joy_sub = rospy.Subscriber('joy',Joy,self.joy_callback,queue_size=1)
		self.twist_pub = rospy.Publisher('/cmd_vel_mux/input/teleop',Twist,queue_size=1)

	def joy_callback(self, joy_msg):
		r = rospy.Rate(5)
		twist = Twist()
		if (joy_msg.buttons[0]==1):
			twist = Twist()
			twist.linear.x = joy_msg.axes[1] * 0.2
			twist.angular.z = 0.0
			self.twist_pub.publish(twist)
		elif (joy_msg.buttons[1]==1):
			twist = Twist()
			twist.linear.x = 0.0
			twist.angular.z = joy_msg.axes[0] * 1.0
			self.twist_pub.publish(twist)
		
		
if __name__ == '__main__':
	rospy.init_node('joymove')
	rate = rospy.Rate(5)
	while not rospy.is_shutdown():
		joy_twist = JoyMove()
		rate.sleep()
