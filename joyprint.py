#! /usr/bin/env python

# -*- coding: utf-8 -*-

import rospy
from sensor_msgs.msg import Joy
from geometry_msgs.msg import Twist
class JoyPrint(object):
	def __init__(self):
		self.joy_sub = rospy.Subscriber('joy',Joy,self.joy_callback,queue_size=1)
		self.twist_sub = rospy.Publisher('/turtle1/cmd_vel',Twist,queue_size=1)

	def joy_callback(self, joy_msg):
		print("--------------------------------")
		print("[0]->"+str(joy_msg.buttons[0]))
		print("[1]->"+str(joy_msg.buttons[1]))
		print("[2]->"+str(joy_msg.buttons[2]))
		print("[3]->"+str(joy_msg.buttons[3]))
		print("[4]->"+str(joy_msg.buttons[4]))
		print("[5]->"+str(joy_msg.buttons[5]))
		print("[6]->"+str(joy_msg.buttons[6]))
		print("[7]->"+str(joy_msg.buttons[7]))
		print("[L_LeftRight]->"+str(joy_msg.axes[0]))
		print("[L_UpDown]->"+str(joy_msg.axes[1]))
		print("[R_LeftRight]->"+str(joy_msg.axes[2]))
		print("[R_UpDown]->"+str(joy_msg.axes[5]))
		
		print("--------------------------------")

if __name__ == '__main__':
	rospy.init_node('joyprint')
	joy_twist = JoyPrint()
	rospy.spin()
