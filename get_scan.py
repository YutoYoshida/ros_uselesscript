#!/usr/bin/env python
# -*- coding: utf-8 -*-
import rospy
import cv2
from sensor_msgs.msg import LaserScan
from cv_bridge import CvBridge, CvBridgeError
import numpy as np


class To_OpenCV():
	def __init__(self):
		self.sub = rospy.Subscriber("/scan", LaserScan, self.get_depth)
		self.bridge = CvBridge()
		self.depth_org = None

	def get_depth(self, data):
		a = np.array(data.ranges)
		print(len(data.ranges))
		print(np.nanmin(a))


if __name__ == '__main__':
	rospy.init_node('To_OpenCV')
	fd = To_OpenCV()

	rate = rospy.Rate(5)
	while not rospy.is_shutdown():
		rospy.loginfo(fd)
		rate.sleep()
