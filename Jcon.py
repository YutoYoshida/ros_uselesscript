#!/usr/bin/env python
#encoding: utf8

import os
import socket
from time import sleep
import re
import threading
import sys


HOST = "localhost"# アドレス
PORT = 10500 # ポート
client = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
client.connect((HOST,PORT))

def J_test():  #juliusの結果受信
	global client
	while(True):
		try:
			while(True):
				#client.send('STATUS\n')
				response = client.recv(1024)   #音声認識結果を文字列として受け取る(xml形式)
				print(response)
				sleep(0.5)   #約0.5秒ごとにループを1回回す
		except:
			client.close()
			sleep(3)
			client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
			client.connect((HOST, PORT))

th = threading.Thread(target=J_test,name="th",args=())
# スレッドthの作成　targetで行いたいメソッド,nameでスレッドの名前,argsで引数を指定する
th.setDaemon(True)
# thをデーモンに設定する。メインスレッドが終了すると、デーモンスレッドは一緒に終了する
th.start()
#スレッドの開始


while(True):
	string = raw_input()
	if 'P' in string:
		client.send('PAUSE\n')

	if 'R' in string:
		client.send('RESUME\n')

	if 'S' in string:
		client.send('STATUS\n')
		response = client.recv(1024)
		print(response)

	if 'q' in string:
		sys.exit()


